module.exports = {
  app_id: '',
  session_key: '',
  api_url: 'https://b2c.jihainet.com/',
  cdn_url: 'https://b2c.jihainet.com/',
  default_image: '',
  app_title: 'Jshop云商',
  app_description: 'Jshop云商',
  app_logo: 'https://wx.qlogo.cn/mmopen/vi_32/PiajxSqBRaEKhQDbaWe1HfkwEibibAHDadWs5Y4ZzVKbms8ksL1jcK601vDIZIUYx2ubmB8SQVFUNFQuVbzFnBy8Q/0',
  list_limit:10,
  image_max:5,
  store_type:2
}